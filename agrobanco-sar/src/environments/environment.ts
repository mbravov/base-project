// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  UrlBase_SGSAPI:  (window as any)["env"]["UrlBase_SGSAPI"],
  UrlBase_SSA: (window as any)["env"]["UrlBase_SSA"],
  UrlBase_SARAPI:  (window as any)["env"]["UrlBase_SARAPI"],
  AppKey:  (window as any)["env"]["AppKey"],
  AppCode :  (window as any)["env"]["AppCode"]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
