import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { Modulo } from '../components/models/modulo.interface';
import { SecurityService } from '../services/security.service';

@Injectable()
export class AuthGuard implements CanActivate {

    public loader = false;
    constructor(private securityService: SecurityService, private router : Router) { }
    
    canActivate() : boolean
    {
        const token = this.securityService.leerTokenSeguridad();        
        this.loader = true;

        if (token === '' || token == null) {
            this.securityService.redirectLogin();
            return false;
        }

        this.securityService.validarToken().subscribe(
           (resp:any)=>{              

               this.securityService.obtenerModulos().subscribe(
                   (result: any) => {
                       
                       //Se valida si entre los modulos que se obtiene, se cuenta con el modulo al cual se quiere acceder
                       var modulos = result.response as Modulo[];
                       let snapshot = this.router.routerState.snapshot;
                       let rutaControllerSolicitada = snapshot.url.split('/')[1];
                       
                    //    if()
                    //    {

                    //    }
                       
                    //    let rutaActionSolicitada = snapshot.url.split('/')[2];
                       
                       console.log('ruta pedida: ' + rutaControllerSolicitada);

                       if (rutaControllerSolicitada != 'home') {
                        var found = false;
                        
                        for (let i = 0; i < modulos.length; i++) {                            
                            const element = modulos[i];
                            let acceso = '';

                            if(element.TipoOpcion == '1' || element.TipoOpcion == '2')
                            {
                                acceso = element.UrlOpcion;
                            }

                            if(element.TipoOpcion == '3')
                            {
                                acceso = element.Controller;
                            }

                            if (acceso.toUpperCase() == rutaControllerSolicitada.toUpperCase()) {
                                found = true;
                                break;
                            }
                        }

                        if (!found) {         
                            this.router.navigate(['error']);                   
                            this.loader = false;
                            return false;
                        }
                       }
                       
                       this.securityService.guardarModulos(modulos);                      
                       
                       this.loader = false;
                       return true;
                   },
                   (error) => {  
                    console.log(error);
                    this.router.navigate(['error']);
                    this.loader = false;
                    return false;
                   });

               return true;
           },
           (error) => {
            console.log(error);
            this.securityService.redirectLogin();
            return false;
          }
        );

        return true;
    }    
}