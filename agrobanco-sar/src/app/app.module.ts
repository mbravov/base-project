import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { MatSelectModule } from '@angular/material/select';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { SidebarComponent } from './components/shared/sidebar/sidebar.component';

import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './components/home/home.component';
import { IndexComponent } from './components/redirect/index/index.component';
import { ErrorComponent } from './components/error/error.component';
import { LoadingComponent } from './components/shared/loading/loading.component';
import { GestionRequerimientoComponent } from './components/gestion-requerimiento/gestion-requerimiento.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { MsgErrorComponent } from './components/shared/msg-error/msg-error.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RequerimientoComponent } from './components/requerimiento/requerimiento.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    HomeComponent,
    IndexComponent,
    ErrorComponent,
    LoadingComponent,
    GestionRequerimientoComponent,
    UsuarioComponent,
    MsgErrorComponent,
    RequerimientoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatTableModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
