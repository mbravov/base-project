import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import {HomeComponent} from './components/home/home.component';
import { IndexComponent } from './components/redirect/index/index.component';
import { ErrorComponent } from './components/error/error.component';
import { GestionRequerimientoComponent } from './components/gestion-requerimiento/gestion-requerimiento.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { RequerimientoComponent } from './components/requerimiento/requerimiento.component';

const routes: Routes = [
  //{ path:'home', component: HomeComponent, canActivate: [AuthGuard] },  
  //{ path:'gestion', component: GestionRequerimientoComponent, canActivate: [AuthGuard]},
  { path:'home', component: HomeComponent},
  { path:'gestion', component: GestionRequerimientoComponent},
  { path: 'gestion/requerimiento', component: RequerimientoComponent},
  { path:'usuario', component: UsuarioComponent, canActivate: [AuthGuard] },
  { path:'error', component: ErrorComponent },  
  { path:'Redirect/Index', component: IndexComponent },  
  { path: '**' , pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
