import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { RequerimientoConsulta } from '../models/requerimientoconsulta.interface';

declare var M:any;
@Component({
  selector: 'app-gestion-requerimiento',
  templateUrl: './gestion-requerimiento.component.html',
  styleUrls: ['./gestion-requerimiento.component.css']
})
export class GestionRequerimientoComponent implements OnInit {

  busquedaFiltro! : FormGroup;
  ELEMENT_DATA! : RequerimientoConsulta[];  
  displayedColumns: string[] = ["NumeroRequerimiento", "editar"];
  listaRequerimiento = new MatTableDataSource<RequerimientoConsulta>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  constructor() { 
    this.busquedaFiltro = new FormGroup({});
  }

  ngOnInit(): void {
    this.inicializarFormulario();
  }

  private inicializarFormulario()
  {
    this.busquedaFiltro = new FormGroup({
      usuarioAsignado : new FormControl('0')
    });
  }


  public buscarListaRequerimiento()
  {

  }

}
